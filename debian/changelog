libqofono (0.114-1ubports1) UNRELEASED; urgency=medium

  * Sync with debian unstable

 -- Guido Berhoerster <guido+ubports@berhoerster.name>  Tue, 21 Jun 2022 13:36:07 +0200

libqofono (0.114-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Jun 2022 12:26:24 +0200

libqofono (0.113-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.6.1. No changes needed.
  * debian/libqofono-qt5-0.symbols:
    + Update symbols.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/rules:
    + Remove x-bits from tests.xml in dh_fixperms override.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 29 May 2022 08:14:06 +0200

libqofono (0.111-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 1002_connman-resetcontexts.patch. Applied upstream.
    + Rebase and extend 2001_path-adjustments.patch. Make sure examples and
      tests land at correct location in the file systems compliant to FHS.
  * debian/{control,rules,*.install}:
    + Convert libqofono-tests and libqofono-examples to Multi-Arch: foreign
      bin:pkgs.
  * debian/control:
    + Drop shlibs:Depends dependency from dev:pkg.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
    + Update copyright of debian/.
  * debian/libqofono-qt5-0.symbols:
    + Update .symbols for 0.111.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 03 Apr 2022 20:59:57 +0000

libqofono (0.106-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update auto-generated copyright.in file.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 08 Mar 2022 20:46:16 +0100

libqofono (0.105-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 02 Feb 2022 08:15:08 +0100

libqofono (0.103-2) unstable; urgency=medium

  * debian/libqofono-qt5-0.symbols:
    + Update symbols for architectures armel, armhf, hurd-i386, i386, m68k,
      mipsel and powerpc. (Closes: #1004405).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 31 Jan 2022 16:03:43 +0100

libqofono (0.103-1) unstable; urgency=medium

  [ Marius Gripsgard ]
  * New upstream version 0.103.

  [ Mike Gabriel ]
  * Initial upload to Debian. (Closes: #1000364).

  * debian/:
    + Add bin:pkgs libqofono-tests and libqofono-examples and adjust some
      paths accordingly. This also adds patch 2001_path-adjustments.patch.
    + Add B-D pkg-kde-tools and build with pkgkde_symbolshelper DH plugin.
      Add .symbols file.
  * debian/{control,compat}:
    + Switch to debhelper-compat notation. Bump to compat level version 13.
  * debian/rules:
    + Switch from --list-missing to --fail-missing and put that into dh_missing
    + Add get-orig-source target (for the maintainers' convenience).
      override.
    + Include dpkg/architecture.mk.
  * debian/watch:
    + Add file.
  * debian/control:
    + Update Maintainer: and Uploaders: fields for Debian upload.
    + Bump Standards-Version: 4.6.0.
    + Update Priority: to 'optional'.
    + Add Vcs-*: fields.
    + Fine-tune SYNOPSIS and LONG_DESCRIPTION fields.
    + Update Homepage: field.
  * debian/libqofono-dev.install:
    + Add .prl file to dev:pkg.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 22 Nov 2021 16:02:17 +0100

libqofono (0.90+16.10.20160901-0ubuntu3) unstable; urgency=medium

  * Remove build dependency on qt5-default, which no longer exists
    (LP: #1921781).

 -- Logan Rosen <logan@ubuntu.com>  Wed, 07 Apr 2021 15:32:59 -0400

libqofono (0.90+16.10.20160901-0ubuntu2) focal; urgency=medium

  * No-change rebuild for libgcc-s1 package name change.

 -- Matthias Klose <doko@ubuntu.com>  Sun, 22 Mar 2020 16:46:34 +0100

libqofono (0.90+16.10.20160917-0ubuntu1) yakkety; urgency=medium

  [ Florian Boucault ]
  * Crossbuild fixes

 -- Robert Bruce Park <robert.park@canonical.com>  Sat, 17 Sep 2016 15:23:54 +0000

libqofono (0.90+16.10.20160901-0ubuntu1) yakkety; urgency=medium

  * New upstream release
  * debian/control
    - Dropped recommends for ofono to a suggests
    - Renamed QML module to follow qml-module-ofono naming (LP: #1342031)

 -- Ken VanDine <ken.vandine@canonical.com>  Thu, 01 Sep 2016 14:59:50 +0000

libqofono (0.82+16.04.20160127-0ubuntu1) xenial; urgency=medium

  * Make ofono dependency optional (LP: #1521142)

 -- Pete Woods <ci-train-bot@canonical.com>  Wed, 27 Jan 2016 14:19:53 +0000

libqofono (0.82+15.10.20150925-0ubuntu1) wily; urgency=medium

  * libqofono-dev should depend on libqofono-qt5-0 (= ${binary:Version}

 -- Ken VanDine <ken.vandine@canonical.com>  Fri, 25 Sep 2015 13:57:19 +0000

libqofono (0.82+15.10.20150911-0ubuntu1) wily; urgency=medium

  [ Ken VanDine ]
  * New upstream release.
  * -debian/patches/expose_modem_tech.patch
    - Dropped, upstream now provides availableTechnologies
  * debian/control
    - Bumped ofono depends to 1.16 for availableTechnologies
  * added packaging added: .bzr-builddeb/ .bzr-builddeb/default.conf
    debian/ debian/changelog debian/compat debian/control
    debian/copyright debian/libqofono-dev.install debian/libqofono-qt5-
    0.install debian/patches/ debian/patches/connman-resetcontexts.patch
    debian/patches/context-preferred.patch
    debian/patches/mtk_settings_binding.patch debian/patches/series
    debian/qtdeclarative5-ofono0.2.install debian/rules debian/source/
    debian/source/format

 -- Tony Espy <ci-train-bot@canonical.com>  Fri, 11 Sep 2015 12:50:11 +0000

libqofono (0.79-0ubuntu2~gcc5.1) wily; urgency=medium

  * No-change test rebuild for g++5 ABI transition

 -- Steve Langasek <steve.langasek@ubuntu.com>  Fri, 17 Jul 2015 22:20:07 +0000

libqofono (0.79-0ubuntu1) wily; urgency=medium

  * New upstream release.
  * debian/patchescontext-preferred.patch
    debian/patches/connman-resetcontexts.patch
    debian/patches/mtk_settings_binding.patch
    debian/patches/expose_modem_tech.patch: Updated

 -- Ken VanDine <ken.vandine@canonical.com>  Fri, 26 Jun 2015 10:16:02 -0400

libqofono (0.70-0ubuntu4) wily; urgency=medium

  [ Jonas Drange ]
  * debian/patches/connman-resetcontexts.patch
    - Added bindings to Ofono's reset context API (LP: #1338758)

 -- Ken VanDine <ken.vandine@canonical.com>  Thu, 21 May 2015 16:16:51 -0400

libqofono (0.70-0ubuntu3) vivid; urgency=medium

  * debian/patches/context-preferred.patch
    - Added bindings to the Ofono ConnectionContext's "Preferred" property,
      patch thanks to Jonas Drange
  * debian/control
    - depend on ofono >= 1.12.bzr6892+15.04.20150407, required for the
    preferred property

 -- Ken VanDine <ken.vandine@canonical.com>  Wed, 22 Apr 2015 10:58:17 -0400

libqofono (0.70-0ubuntu2) vivid; urgency=medium

  * debian/control
    - fixed typo in breaks

 -- Ken VanDine <ken.vandine@canonical.com>  Wed, 14 Jan 2015 09:36:03 -0500

libqofono (0.70-0ubuntu1) vivid; urgency=medium

  * New upstream release.
  * Dropped patches applied upstream, service_numbers.patch and
    fix_multiarch.patch
  * Refreshed expose_modem_tech.patch and mtk_settings_binding.patch

 -- Ken VanDine <ken.vandine@canonical.com>  Mon, 12 Jan 2015 11:32:24 -0500

libqofono (0.53-0ubuntu3) vivid; urgency=medium

  * debian/patches/mtk_settings_binding.patch
    - add mtk (mediatek) settings binding which allows consumer
      to set which modem to allow 3G communication (LP: #1373388)

 -- Ken VanDine <ken.vandine@canonical.com>  Fri, 21 Nov 2014 10:07:54 -0500

libqofono (0.53-0ubuntu2) utopic; urgency=medium

  * debian/patches/expose_modem_tech.patch
    - radiosettings: expose modemtechnologies (LP: #1346790)

 -- Ken VanDine <ken.vandine@canonical.com>  Wed, 20 Aug 2014 11:17:52 -0400

libqofono (0.53-0ubuntu1) utopic; urgency=low

  [ Ken VanDine ]
  * New package

  [ Łukasz 'sil2100' Zemczak ]
  * debian/patches/fix_multiarch.patch:
    - Fix qmake files to enable proper multiarch support during package build.
  * Prepare proper copyright information

 -- Ken VanDine <ken.vandine@canonical.com>  Wed, 18 Jun 2014 13:02:42 -0400
